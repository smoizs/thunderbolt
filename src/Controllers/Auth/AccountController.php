<?php
namespace Conceptlz\Zeus\Thunderbolt\Controllers\Auth;

use Carbon\Carbon;
use Conceptlz\Zeus\Thunderbolt\Controllers\BaseController;
use Conceptlz\Zeus\Thunderbolt\Models\Verification;

class AccountController extends BaseController
{
    public function activate($token)
    {
        if( $verification = Verification::findByToken($token) ) {
            // get the difference in token creation
            $diffInMinutes = $verification->created_at->diffInMinutes(Carbon::now());
            $expiresInMinutes = config('thunderbolt.auth.activation_link_expires', 60);

            // check for link expired
            if( $diffInMinutes > $expiresInMinutes) {
                return redirect()->route('login')
                    ->with('status', trans('thunderbolt::auth.token_expired'));
            }

            // get the user and activate be clearing token for user
            if( $user = $verification->user) {
                $verification->delete();

                return redirect()->route('login')
                    ->with('status', trans('thunderbolt::auth.email_confirmed', ['name' => $user->name]));
            }
        }

        return redirect()->route('login')
                ->with('status', trans('thunderbolt::auth.invalid_token'));
    }
}
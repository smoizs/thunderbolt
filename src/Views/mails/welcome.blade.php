@component('mail::message')
## Welcome aboard {{ $user->name }}

Your account has been created.

@if( $verification = $user->verification)
Please confirm your email address {{ $user->email }} by clicking below button.
@php $link = route('activate_account', $verification->token) @endphp

@component('mail::button', ['url' => $link, 'color' => 'green'])
    Confirm Email
@endcomponent

@component('mail::panel')
    {{ $link }}
@endcomponent
@endif

Thanks

{{ config('app.name') }}
@endcomponent
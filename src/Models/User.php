<?php
namespace Conceptlz\Zeus\Thunderbolt\Models;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as LaravelUser;

class User extends LaravelUser {
    use HasRoles, Notifiable;

    protected $guarded = [];

    public function verification()
    {
        return $this->hasOne(Verification::class);
    }
}